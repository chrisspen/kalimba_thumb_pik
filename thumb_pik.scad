
module make_thumb_pik_base(d){
    scale([1,.7,2.5])
    sphere(d=d, $fn=100);
}

module make_thumb_pik(d=20+2){
    difference(){
        union(){
            difference(){
            
                if(1)
                intersection(){
                    make_thumb_pik_base(d=d);

                    color("grey")
                    //translate([20,0,0])
                    translate([0,0,d*2/2])
                    cube([d+5,d+5,d*2], center=true);
                }
                
                // interior cutout
                intersection(){
                    make_thumb_pik_base(d=d-2);
                    
                    color("grey")
                    //translate([20,0,0])
                    translate([0,0,20/2])
                    cube([d+5,14+5,d], center=true);
                }
                
                color("red")
                translate([-100/2,-100/2,22])
                cube([100,100,100]);
                
            }//end diff
            
            if(1){
                translate([0,0,21.75]){
                    difference(){
                        
                        scale([3.3,2.3,1.1])
                        sphere(d=4, $fn=100);
                    
                        color("red")
                        translate([-100/2,-100/2,-100])
                        cube([100,100,100]);
                    }
                }
            }
        }

        color("red")
        translate([0,3,24])
        scale([2,1,1])
        sphere(d=4, $fn=100);
    
        color("red")
        translate([0,0,-100/2+5])
        cube([100,100,100], center=true);
    }
}

if(1)
difference(){
    //translate([0,0,20/2])
    make_thumb_pik();

    if(0)
    color("red")
    translate([0,0,-100/2])
    cube([100,100,100]);
}

